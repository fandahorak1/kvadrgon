#verze 1.3
zakl_map = [
    [[0, 0, 3], [0, 1, 3], [0, 2, 1], [0, 3, 0], [0, 4, 0], [0, 5, 0], [0, 6, 2], [0, 7, 3], [0, 8, 3], [0, 9, 3]],
    [[1, 0.5, 3], [1, 1.5, 0], [1, 2.5, 0], [1, 3.5, 0], [1, 4.5, 0], [1, 5.5, 0], [1, 6.5, 0], [1, 7.5, 3],
     [1, 8.5, 3], [1, 9.5, 3]],
    [[2, 0, 3], [2, 1, 0], [2, 2, 0], [2, 3, 0], [2, 4, 0], [2, 5, 0], [2, 6, 0], [2, 7, 0], [2, 8, 3], [2, 9, 3]],
    [[3, 0.5, 0], [3, 1.5, 0], [3, 2.5, 0], [3, 3.5, 0], [3, 4.5, 3], [3, 5.5, 0], [3, 6.5, 0], [3, 7.5, 0],
     [3, 8.5, 3], [3, 9.5, 3]],
    [[4, 0, 2], [4, 1, 0], [4, 2, 0], [4, 3, 3], [4, 4, 0], [4, 5, 0], [4, 6, 0], [4, 7, 0], [4, 8, 1], [4, 9, 3]],
    [[5, 0.5, 0], [5, 1.5, 0], [5, 2.5, 0], [5, 3.5, 0], [5, 4.5, 3], [5, 5.5, 0], [5, 6.5, 0], [5, 7.5, 0],
     [5, 8.5, 3], [5, 9.5, 3]],
    [[6, 0, 3], [6, 1, 0], [6, 2, 0], [6, 3, 0], [6, 4, 0], [6, 5, 0], [6, 6, 0], [6, 7, 0], [6, 8, 3], [6, 9, 3]],
    [[7, 0.5, 3], [7, 1.5, 0], [7, 2.5, 0], [7, 3.5, 0], [7, 4.5, 0], [7, 5.5, 0], [7, 6.5, 0], [7, 7.5, 3],
     [7, 8.5, 3], [7, 9.5, 3]],
    [[8, 0, 3], [8, 1, 3], [8, 2, 1], [8, 3, 0], [8, 4, 0], [8, 5, 0], [8, 6, 2], [8, 7, 3], [8, 8, 3], [8, 9, 3]],
    [[9, 0.5, 3], [9, 1.5, 3], [9, 2.5, 3], [9, 3.5, 3], [9, 4.5, 3], [9, 5.5, 3], [9, 6.5, 3], [9, 7.5, 3],
     [9, 8.5, 3], [9, 9.5, 3]]]

x_off = 0
y_off = 50
import os

os.environ['SDL_VIDEO_WINDOW_POS'] = "%d,%d" % (x_off, y_off)

import sys

import pygame
from pygame.locals import *
# soubor
from ast import literal_eval

import os.path
from os import path

if getattr(sys, 'frozen', False):
    # If the application is run as a bundle, the PyInstaller bootloader
    # extends the sys module by a flag frozen=True and sets the app
    # path into variable _MEIPASS'.
    application_path = ""
else:
    application_path = os.path.dirname(os.path.abspath(__file__)) + "/"

pygame.init()

infoObject = pygame.display.Info()  # parametry monitoru
pygame.mouse.set_cursor((8, 8), (0, 0), (0, 0, 0, 0, 0, 0, 0, 0), (0, 0, 0, 0, 0, 0, 0, 0))
clock = pygame.time.Clock()  # hodiny

myfont = pygame.font.SysFont('arial', 60)  # nastaveni textu
font_cisel = pygame.font.SysFont('calibri', 40)  # nastaveni textu

red_win = pygame.image.load("rub_vyhr.png")
blue_win = pygame.image.load("perl_vyhr.png")
remiza = pygame.image.load("remiza.png")

screen_y = infoObject.current_h
screen_x = infoObject.current_w

half_y = int(screen_y / 2)
half_x = int(screen_x / 2)

pocet_rub_pruh = pygame.Rect(0, 0, 50, 40)
pocet_perl_pruh = pygame.Rect(screen_y, 0, 50, 40)

per_sound = 'perla.mp3'
rub_sound = 'rubin.mp3'
kon_sound = pygame.mixer.Sound("konec.mp3")

voice = pygame.mixer.Channel(5)

pygame.mixer.init()

n = []
hexxagon = pygame.image.load("kvadrgon.png")
rub = pygame.image.load("rubin.png")
perl = pygame.image.load("perla.png")
zlut = pygame.image.load("zluty.png")
zel = pygame.image.load("zeleny.png")
oran = pygame.image.load("orandzovy.png")

sip_cer = pygame.image.load("sipka_cer.png")
sip_mod = pygame.image.load("sipka_mod.png")

x_tmp = []
y_tmp = []
prem = [[0], [0, 0]]
x = 0
y = 0
div = 0
coor = [0, 0]
z = 0
hraje = 1
pocet_jedn = [0, 0, 0]
ctverce = []
sloupce = 15
radky = 8
vybrany = []
klikl = False
v_menu = True
konec = False
poprve = True

vsechny_kruhy = []

celk_pocet_poli = 2

rub_pocet = font_cisel.render(str(pocet_jedn[1]), False, (255, 0, 0))
perl_pocet = font_cisel.render(str(pocet_jedn[2]), False, (0, 0, 255))

vel_pol_x = 64
vel_pol_y = 64

scr_offset_y = 0
scr_offset_x = 30

ctver_vel_y = 50
ctver_vel_x = 50
fullscreen = True
hex_trans = pygame.transform.scale(hexxagon, (ctver_vel_y, ctver_vel_x))


def resize():
    global screen_y, screen_x, scr_offset_x, scr_offset_y, half_y, half_x, ctver_vel_y, ctver_vel_x, hex_trans, \
        rub_trans, perl_trans, zlut_trans, zel_trans, oran_trans, s_mod_tr, s_cer_tr
    half_y = int(screen_y / 2)
    half_x = int(screen_x / 2)
    ctver_vel_y = int(screen_y / (radky + 1) - (screen_y * 0.025) / 2)

    ctver_vel_x = int(screen_y / (sloupce + 1))
    hex_trans = pygame.transform.scale(hexxagon, (ctver_vel_x + int(screen_y * 0.025), ctver_vel_y))
    rub_trans = pygame.transform.scale(rub, (ctver_vel_x + int(screen_y * 0.025), ctver_vel_y))
    perl_trans = pygame.transform.scale(perl, (ctver_vel_x + int(screen_y * 0.025), ctver_vel_y))
    zlut_trans = pygame.transform.scale(zlut, (ctver_vel_x + int(screen_y * 0.025), ctver_vel_y))

    zel_trans = pygame.transform.scale(zel, (ctver_vel_x + int(screen_y * 0.025), ctver_vel_y))
    oran_trans = pygame.transform.scale(oran, (ctver_vel_x + int(screen_y * 0.025), ctver_vel_y))

    s_cer_tr = pygame.transform.scale(sip_cer, (ctver_vel_x, ctver_vel_y))
    s_mod_tr = pygame.transform.scale(sip_mod, (ctver_vel_x, ctver_vel_y))

    scr_offset_y = 40
    scr_offset_x = 0

    tmp_offset = int((screen_y - (ctver_vel_y * radky)) / 2)
    if tmp_offset > 0:
        scr_offset_y += tmp_offset
    tmp_offset = int((screen_x - (ctver_vel_x * sloupce)) / 2)
    if tmp_offset > 0:
        scr_offset_x += tmp_offset


resize()

tag = [pygame.FULLSCREEN]

win = pygame.display.set_mode((screen_y, screen_x), tag[0])

win.fill((255, 255, 255))

# nastaveni gui
gray = (100, 100, 100)
gray2 = [220, 220, 220]
BLUE = (0, 0, 255)

# nacteni ze souboru
load_map = False
path_proj = application_path
ml = []
con_hraje = 1
corrupt_map = False
corrupted_config = False
vykresli = True
i = 0
vystup = 0
but_height = int(screen_y / 10)
but_width = int(screen_x / 8)
but_width_s = int(screen_x / 15)
print(but_height, but_width, but_width_s)


# noinspection PyBroadException
def str_int(vstup):
    global i, vystup
    i = 0
    vystup = 0
    vstup = vstup[::-1]
    for p in vstup:
        if p == "=":
            break
        i += 1
        try:
            vystup += int(p) * (10 ** i)
        except:
            i -= 1
    return int(vystup / 10)


with open(path_proj + 'config/config.txt') as f:
    soubor = f.readlines()
    for row in soubor:
        if row == "file_load = True\n":
            load_map = True

        if row == "file_load = False\n":
            load_map = True
        if row.find("hraje = ") != -1:
            hraje = int(row[8])
        if row.find("radky = ") != -1:
            radky = str_int(row)
        if row.find("sloupce = ") != -1:
            sloupce = str_int(row)

if load_map:
    try:
        with open(path_proj + 'config/list.txt') as f:
            mainlist = [list(literal_eval(line)) for line in f]

        for x in range(sloupce + 1):
            for y in range(radky + 1):
                if len(mainlist[x][y]) != 3:
                    corrupt_map = True
        if not corrupt_map:
            ctverce += mainlist
    except:
        print("failed to load")
        corrupt_map = True


def vytv_map(nacist_nastaveni):
    global ctverce, sloupce, radky, x, y, hraje, scr_offset_y, scr_offset_x, load_map
    if nacist_nastaveni:
        with open(path_proj + 'config/config.txt') as f:
            soubor = f.readlines()
            for row in soubor:
                if row == "file_load = True\n":
                    load_map = True
                if row == "file_load = False\n":
                    load_map = False
                if row.find("hraje = ") != -1:
                    hraje = int(row[8])
                if row.find("radky = ") != -1:
                    radky = str_int(row)
                if row.find("sloupce = ") != -1:
                    sloupce = str_int(row)

    if not load_map or corrupt_map:
        print("generace mapy 1")
        ctverce.clear()
        radek = []
        tmp = 0
        typ = 0
        hraje = 1
        for y in range(0, sloupce + 1):
            for x in range(0, radky + 1):
                tmp += 32
                if x >= radky or y >= sloupce:
                    typ = 3
                if y == int(sloupce / 2) and x == radky - 1:
                    typ = 1
                if y == int(sloupce / 2) and x == 0:
                    typ = 2
                if y % 2 == 0:
                    radek.append([y, x, typ])
                else:
                    radek.append([y, x + 0.5, typ])
                typ = 0
            ctverce.append(radek)
            radek = []
        ctverce.clear()
        ctverce += zakl_map
    if load_map and not corrupt_map:
        ctverce.clear()
        with open(path_proj + 'config/list.txt') as f:
            mainlist = [list(literal_eval(line)) for line in f]
        ctverce += mainlist
        print("nacitani mapy")


if not load_map:
    vytv_map(True)
    with open(path_proj + 'config/list.txt', "w") as f:
        for row in ctverce:
            f.write(str(row) + "\n")
    print("vytvareni mapy 2")

txt_siz = int(screen_x / 50)
print(txt_siz)


class button:
    def __init__(self, position, size_b, clr=gray, cngclr=None, func=None, text_b='', font_b="Arial",
                 font_size_button=txt_siz, font_clr=(0, 0, 0)):
        self.clr = clr
        self.size = size_b
        self.func = func
        self.surf = pygame.Surface(size_b)
        self.rect = self.surf.get_rect(center=position)

        if cngclr:
            self.cngclr = cngclr
        else:
            self.cngclr = clr

        if len(clr) == 4:
            self.surf.set_alpha(clr[3])

        self.font = pygame.font.SysFont(font_b, font_size_button)
        self.txt = text_b
        self.font_clr = font_clr
        self.txt_surf = self.font.render(self.txt, True, self.font_clr)
        self.txt_rect = self.txt_surf.get_rect(center=[wh // 2 for wh in self.size])

    def draw(self, screen):
        self.mouseover()

        self.surf.fill(self.curclr)
        self.surf.blit(self.txt_surf, self.txt_rect)
        screen.blit(self.surf, self.rect)

    def mouseover(self):
        self.curclr = self.clr
        pos_mys = pygame.mouse.get_pos()
        if self.rect.collidepoint(pos_mys):
            self.curclr = self.cngclr

    def call_back(self, *args):
        if self.func:
            return self.func(*args)


class text:
    def __init__(self, msg, position, clr=gray, font_tlacitka="Arial", font_size_tlacitka=15, mid=False):
        self.position = position
        self.font = pygame.font.SysFont(font_tlacitka, font_size_tlacitka)
        self.txt_surf = self.font.render(msg, True, clr)

        if len(clr) == 4:
            self.txt_surf.set_alpha(clr[3])

        if mid:
            self.position = self.txt_surf.get_rect(center=position)

    def draw(self, screen):
        screen.blit(self.txt_surf, self.position)


# call back functions
def fn1():
    pygame.quit()
    sys.exit()


def fn2():
    global v_menu
    v_menu = False


def fn3():
    global v_menu, vybrany, ctverce
    v_menu = False
    vytv_map(True)
    kolik_poli()
    vybrany.clear()


def fn4():
    global v_menu
    v_menu = True


def ulozeni_hry():
    with open(path_proj + 'config/list.txt', "w") as f:
        for row in ctverce:
            f.write(str(row) + "\n")
    a_file = open(path_proj + "config/config.txt", "r")
    list_of_lines = a_file.readlines()
    list_of_lines[0] = "file_load = " + str(load_map) + "\n"

    list_of_lines[1] = "hraje = " + str(hraje) + "\n"

    a_file = open(path_proj + "config/config.txt", "w")
    a_file.writelines(list_of_lines)
    a_file.close()
    pygame.quit()
    sys.exit()


def reset_hra():
    global v_menu, load_map, hraje, pocet_jedn, konec
    konec = False
    pocet_jedn = [0, 0, 0]
    load_map = False
    hraje = 1
    v_menu = False
    vytv_map(False)
    kolik_poli()
    vybrany.clear()


def reset_set():
    global v_menu, load_map, hraje
    a_file = open(path_proj + "config/config.txt", "r")
    list_of_lines = a_file.readlines()
    list_of_lines[0] = "file_load = " + str(load_map) + "\n"
    list_of_lines[1] = "hraje = " + str(hraje) + "\n"
    list_of_lines[2] = "radky = " + str(radky) + "\n"
    list_of_lines[3] = "sloupce = " + str(sloupce) + "\n"
    a_file = open(path_proj + "config/config.txt", "w")
    a_file.writelines(list_of_lines)

    a_file.close()


def ed_pol():
    global hraje
    hraje = 0


def ed_rub():
    global hraje
    hraje = 1


def ed_per():
    global hraje
    hraje = 2


def ed_pra():
    global hraje
    hraje = 3


toggl = load_map

edit = False


def edit_togg():
    global toggl, togg, menu_tl, load_map, e, edit, hraje
    edit = not edit
    button_load()
    if edit:
        e = button((but_width * 2, 0 + int(but_height / 2)), (but_width, but_height), (0, 255, 0), (0, 200, 0),
                   edit_togg, 'edit')
    elif not edit:
        hraje = 1
        e = button((but_width * 2, 0 + int(but_height / 2)), (but_width, but_height), (210, 0, 0), (150, 0, 0),
                   edit_togg, 'edit')
    s = button(position=(int(but_width / 2), screen_y - int(but_height / 2)), size_b=(but_width, but_height), clr=gray2,
               cngclr=(200, 0, 0), func=fn1,
               text_b='opustit hru')
    p = button((int(half_x / 1.37 * 2), screen_y - int(but_height / 2)), (but_width, but_height), gray2, (200, 0, 0),
               fn2, 'pokračovat')
    z = button((screen_x - int((but_width * 1.5) / 2), screen_y - int(but_height / 2)),
               (int(but_width * 1.5), but_height), gray2, (200, 0, 0), reset_hra, 'hrát znovu')
    u = button((int(half_x / 1.55), screen_y - int(but_height / 2)), (int(but_width * 1.5), but_height), gray2,
               (200, 0, 0), ulozeni_hry, 'uložit a opustit hru')
    r = button((int(screen_x / 1.85), screen_y - int(but_height / 2)), (int(but_width * 1.5), but_height), gray2,
               (200, 0, 0), reset_set, 'resetovat nastavení')
    menu_tl.clear()
    menu_tl = [s, p, z, u, r, e, togg]


def toggle():
    global toggl, togg, menu_tl, load_map, e
    toggl = not toggl

    if edit:
        e = button((but_width * 2, 0 + int(but_height / 2)), (but_width, but_height), (0, 255, 0), (0, 200, 0),
                   edit_togg, 'edit')
    elif not edit:
        e = button((but_width * 2, 0 + int(but_height / 2)), (but_width, but_height), (210, 0, 0), (150, 0, 0),
                   edit_togg, 'edit')
    if toggl:
        togg = button((half_x, int(but_height / 2)), (int(but_width * 1.2), but_height), (0, 255, 0), (0, 200, 0),
                      toggle,
                      'nacist mapu')
        load_map = True
    elif not toggl:
        togg = button((half_x, int(but_height / 2)), (int(but_width * 1.2), but_height), (210, 0, 0), (150, 0, 0),
                      toggle,
                      'vytvorit mapu')
        load_map = False

    s = button(position=(int(but_width / 2), screen_y - int(but_height / 2)), size_b=(but_width, but_height), clr=gray2,
               cngclr=(200, 0, 0), func=fn1,
               text_b='opustit hru')
    p = button((int(half_x / 1.37 * 2), screen_y - int(but_height / 2)), (but_width, but_height), gray2, (200, 0, 0),
               fn2, 'pokračovat')

    z = button((screen_x - int((but_width * 1.5) / 2), screen_y - int(but_height / 2)),
               (int(but_width * 1.5), but_height), gray2, (200, 0, 0), reset_hra, 'hrát znovu')
    u = button((int(half_x / 1.55), screen_y - int(but_height / 2)), (int(but_width * 1.5), but_height), gray2,
               (200, 0, 0), ulozeni_hry, 'uložit a opustit hru')
    r = button((int(screen_x / 1.85), screen_y - int(but_height / 2)), (int(but_width * 1.5), but_height), gray2,
               (200, 0, 0), reset_set, 'resetovat nastavení')
    menu_tl.clear()
    menu_tl = [s, p, z, u, r, e, togg]


togg = button((half_y, 25), (but_width, but_height), (0, 255, 0), (0, 200, 0), toggle, 'nacist mapu')

if True:
    size = 10
    font_size = 15
    font = pygame.font.Font(None, font_size)
menu_tl = []
hra_tl = []


def button_load():
    global menu_tl, hra_tl, toggl, edit, togg, e
    hra_tl.clear()

    s = button(position=(int(but_width / 2), screen_y - int(but_height / 2)), size_b=(but_width, but_height), clr=gray2,
               cngclr=(200, 0, 0), func=fn1,
               text_b='opustit hru')
    p = button((int(half_x / 1.37 * 2), screen_y - int(but_height / 2)), (but_width, but_height), gray2, (200, 0, 0),
               fn2, 'pokračovat')

    z = button((screen_x - int((but_width * 1.5) / 2), screen_y - int(but_height / 2)),
               (int(but_width * 1.5), but_height), gray2, (200, 0, 0), reset_hra, 'hrát znovu')
    u = button((int(half_x / 1.55), screen_y - int(but_height / 2)), (int(but_width * 1.5), but_height), gray2,
               (200, 0, 0), ulozeni_hry,
               'uložit a opustit hru')
    r = button((int(screen_x / 1.85), screen_y - int(but_height / 2)), (int(but_width * 1.5), but_height), gray2,
               (200, 0, 0), reset_set,
               'resetovat nastavení')

    m = button((int(but_width / 4), screen_y - int(but_height / 3)), (int(but_width / 1.2), int(but_height / 1.4)),
               [255, 100, 100], (200, 0, 0), fn4, 'menu')

    if edit:
        po = button((int(but_width / 2), int(but_height / 2) + 30), (but_width, int(but_height / 1.2)),
                    [255, 100, 100], (200, 0, 0), ed_pol, 'policko')
        ru = button((int(but_width / 2) + but_width, int(but_height / 2) + 30), (but_width, int(but_height / 1.2)),
                    [255, 100, 100], (200, 0, 0), ed_rub, 'rubín')
        pe = button((int(but_width / 2) + but_width * 2, int(but_height / 2) + 30), (but_width, int(but_height / 1.2)),
                    [255, 100, 100], (200, 0, 0), ed_per, 'perla')
        pr = button((int(but_width / 2) + but_width * 3, int(but_height / 2) + 30), (but_width, int(but_height / 1.2)),
                    [255, 100, 100], (200, 0, 0), ed_pra, 'prázdné')
        hra_tl = [m, po, ru, pe, pr]
    elif not edit:
        hra_tl = [m]

    if edit:
        e = button((but_width * 2, 0 + int(but_height / 2)), (but_width, but_height), (0, 255, 0), (0, 200, 0),
                   edit_togg, 'edit')
    elif not edit:
        e = button((but_width * 2, 0 + int(but_height / 2)), (but_width, but_height), (210, 0, 0), (150, 0, 0),
                   edit_togg, 'edit')

    if toggl:
        togg = button((half_x, int(but_height / 2)), (int(but_width * 1.2), but_height), (0, 255, 0), (0, 200, 0),
                      toggle, 'nacist mapu')
    elif not toggl:
        togg = button((half_x, int(but_height / 2)), (int(but_width * 1.2), but_height), (210, 0, 0), (150, 0, 0),
                      toggle, 'vytvorit mapu')

    menu_tl.clear()

    menu_tl = [s, p, z, u, r, e, togg]


n = myfont.render(str(pygame.mouse.get_pos()), False, (0, 0, 255))

predtim = False


def menu():
    global v_menu, ctverce, b, screen_x, screen_y, win, menu_tl, hra_tl, poprve, fullscreen, predtim
    vykresli = False

    ni = myfont.render(str(pygame.mouse.get_pos()) + str(edit), False, (0, 0, 255))
    if poprve:
        screen_y = infoObject.current_h
        screen_x = infoObject.current_w
        poprve = False
        win = pygame.display.set_mode((screen_x, screen_y), tag[0])
        resize()
        button_load()
        win.fill((255, 0, 0))
        for b in menu_tl:
            b.draw(win)
        pygame.display.update()
    for ev in pygame.event.get():
        if ev.type == pygame.QUIT:
            pygame.quit()
            sys.exit()
        elif ev.type == pygame.MOUSEBUTTONDOWN:
            if ev.button == 1:
                pose = pygame.mouse.get_pos()
                for b in menu_tl:
                    if b.rect.collidepoint(pose):
                        b.call_back()
    posi = pygame.mouse.get_pos()
    for b in menu_tl:
        if b.rect.collidepoint(posi):
            vykresli = True

    if vykresli or predtim or True:
        win.fill((255, 0, 0))
        for b in menu_tl:
            b.draw(win)
        # win.blit(ni, (0, 0))  # pozice myši
        x, y = pygame.mouse.get_pos()
        x -= int(s_cer_tr.get_width() / 2)
        y -= 10
        win.blit(s_mod_tr, (x, y))
        pygame.display.update()
    if predtim:
        predtim = False
    if vykresli:
        predtim = True

    clock.tick(30)


def kolik_poli():
    global pocet_jedn, y, x
    pocet_jedn = [0, 0, 0]
    for y in range(0, sloupce):
        for x in range(0, radky):
            if ctverce[y][x][2] == 0:
                pocet_jedn[0] += 1

            if ctverce[y][x][2] == 1:
                pocet_jedn[1] += 1
            if ctverce[y][x][2] == 2:
                pocet_jedn[2] += 1


def zobraz():
    # test jestli někdo vyhrál
    global win, z, x, y, div, pocet_jedn, rub_pocet, perl_pocet, celk_pocet_poli, i
    # kresleni
    vsechny_kruhy.clear()
    vytv_kruhy()
    win.fill((255, 255, 255))
    for i in range(0, sloupce):
        for z in range(0, radky):
            if ctverce[i][z][2] != 3:
                x = ctverce[i][z][0]
                y = ctverce[i][z][1]
                win.blit(hex_trans, (x * ctver_vel_x + scr_offset_x, y * ctver_vel_y + scr_offset_y))
                if ctverce[i][z][2] == 1:
                    win.blit(rub_trans, (x * ctver_vel_x + scr_offset_x, y * ctver_vel_y + scr_offset_y))
                if ctverce[i][z][2] == 2:
                    win.blit(perl_trans, (x * ctver_vel_x + scr_offset_x, y * ctver_vel_y + scr_offset_y))

    if vybrany and not edit:
        x = ctverce[vybrany[1]][vybrany[0]][0]
        y = ctverce[vybrany[1]][vybrany[0]][1]
        win.blit(zel_trans,
                 (x * ctver_vel_x + scr_offset_x, y * ctver_vel_y + scr_offset_y))  # oznaceni vybraneho_policka
        div = (vybrany[1] + 1) % 2
        if ctverce[vybrany[1]][vybrany[0] - 1][2] == 0:  # sikmo
            x = ctverce[vybrany[1]][vybrany[0] - 1][0]
            y = ctverce[vybrany[1]][vybrany[0] - 1][1]
            win.blit(zlut_trans, (x * ctver_vel_x + scr_offset_x, y * ctver_vel_y + scr_offset_y))

        if ctverce[vybrany[1] + 1][vybrany[0]][2] == 0:  # doprava
            x = ctverce[vybrany[1] + 1][vybrany[0]][0]
            y = ctverce[vybrany[1] + 1][vybrany[0]][1]
            win.blit(zlut_trans, (x * ctver_vel_x + scr_offset_x, y * ctver_vel_y + scr_offset_y))
        if ctverce[vybrany[1] - 1][vybrany[0]][2] == 0:  # doleva
            x = ctverce[vybrany[1] - 1][vybrany[0]][0]
            y = ctverce[vybrany[1] - 1][vybrany[0]][1]
            win.blit(zlut_trans, (x * ctver_vel_x + scr_offset_x, y * ctver_vel_y + scr_offset_y))

        div = (vybrany[1] + 1) % 2
        if ctverce[vybrany[1] - div][vybrany[0] - 1][2] == 0:  # sikmo
            x = ctverce[vybrany[1] - div][vybrany[0] - 1][0]
            y = ctverce[vybrany[1] - div][vybrany[0] - 1][1]
            win.blit(zlut_trans, (x * ctver_vel_x + scr_offset_x, y * ctver_vel_y + scr_offset_y))
        div = (vybrany[1] + 1) % 2
        if ctverce[vybrany[1] + 1][vybrany[0] + 1][2] == 0:  # sikmo
            x = ctverce[vybrany[1] + 1][vybrany[0] + 1][0]
            y = ctverce[vybrany[1] + 1][vybrany[0] + 1][1]
            win.blit(zlut_trans, (x * ctver_vel_x + scr_offset_x, y * ctver_vel_y + scr_offset_y))
        div = (vybrany[1] + 1) % 2
        if ctverce[vybrany[1] + 1][vybrany[0] - 1][2] == 0:  # sikmo
            x = ctverce[vybrany[1] + 1][vybrany[0] - 1][0]
            y = ctverce[vybrany[1] + 1][vybrany[0] - 1][1]
            win.blit(zlut_trans, (x * ctver_vel_x + scr_offset_x, y * ctver_vel_y + scr_offset_y))

        div = (vybrany[1] + 1) % 2
        if ctverce[vybrany[1] + div][vybrany[0] + 1][2] == 0:  # sikmo
            x = ctverce[vybrany[1] + div][vybrany[0] + 1][0]
            y = ctverce[vybrany[1] + div][vybrany[0] + 1][1]
            win.blit(zlut_trans, (x * ctver_vel_x + scr_offset_x, y * ctver_vel_y + scr_offset_y))
        if ctverce[vybrany[1] - 1 + div][vybrany[0] + 1][2] == 0:  # sikmo
            x = ctverce[vybrany[1] - 1 + div][vybrany[0] + 1][0]
            y = ctverce[vybrany[1] - 1 + div][vybrany[0] + 1][1]
            win.blit(zlut_trans, (x * ctver_vel_x + scr_offset_x, y * ctver_vel_y + scr_offset_y))

        div = (vybrany[1]) % 2
        if vybrany[0] < radky and 0 < vybrany[1] < sloupce:
            if ctverce[vybrany[1] - 2][vybrany[0] + 1][2] == 0:
                x = ctverce[vybrany[1] - 2][vybrany[0] + 1][0]
                y = ctverce[vybrany[1] - 2][vybrany[0] + 1][1]
                win.blit(oran_trans, (x * ctver_vel_x + scr_offset_x, y * ctver_vel_y + scr_offset_y))
        if vybrany[0] < radky and 0 < vybrany[1] < sloupce:
            if ctverce[vybrany[1] - 2][vybrany[0]][2] == 0:
                x = ctverce[vybrany[1] - 2][vybrany[0]][0]
                y = ctverce[vybrany[1] - 2][vybrany[0]][1]
                win.blit(oran_trans, (x * ctver_vel_x + scr_offset_x, y * ctver_vel_y + scr_offset_y))
        if 0 < vybrany[0] and 0 < vybrany[1] < sloupce:
            if ctverce[vybrany[1] - 2][vybrany[0] - 1][2] == 0:
                x = ctverce[vybrany[1] - 2][vybrany[0] - 1][0]
                y = ctverce[vybrany[1] - 2][vybrany[0] - 1][1]
                win.blit(oran_trans, (x * ctver_vel_x + scr_offset_x, y * ctver_vel_y + scr_offset_y))

        div = (vybrany[1] + 1) % 2
        if vybrany[0] < radky and vybrany[1] < sloupce - 1:

            if ctverce[vybrany[1] + 2][vybrany[0] + 1][2] == 0:
                x = ctverce[vybrany[1] + 2][vybrany[0] + 1][0]
                y = ctverce[vybrany[1] + 2][vybrany[0] + 1][1]
                win.blit(oran_trans, (x * ctver_vel_x + scr_offset_x, y * ctver_vel_y + scr_offset_y))

            if ctverce[vybrany[1] + 2][vybrany[0]][2] == 0:
                x = ctverce[vybrany[1] + 2][vybrany[0]][0]
                y = ctverce[vybrany[1] + 2][vybrany[0]][1]
                win.blit(oran_trans, (x * ctver_vel_x + scr_offset_x, y * ctver_vel_y + scr_offset_y))
        if 0 < vybrany[0] < radky and vybrany[1] < sloupce - 1:
            if ctverce[vybrany[1] + 2][vybrany[0] - 1][2] == 0:
                x = ctverce[vybrany[1] + 2][vybrany[0] - 1][0]
                y = ctverce[vybrany[1] + 2][vybrany[0] - 1][1]
                win.blit(oran_trans, (x * ctver_vel_x + scr_offset_x, y * ctver_vel_y + scr_offset_y))
        div = (vybrany[1]) % 2
        if 0 < vybrany[0] < radky and vybrany[1] < sloupce:
            if ctverce[vybrany[1] + 1][vybrany[0] - 2 + div][2] == 0:
                x = ctverce[vybrany[1] + 1][vybrany[0] - 2 + div][0]
                y = ctverce[vybrany[1] + 1][vybrany[0] - 2 + div][1]
                win.blit(oran_trans, (x * ctver_vel_x + scr_offset_x, y * ctver_vel_y + scr_offset_y))
            if ctverce[vybrany[1]][vybrany[0] - 2][2] == 0:
                x = ctverce[vybrany[1]][vybrany[0] - 2][0]
                y = ctverce[vybrany[1]][vybrany[0] - 2][1]
                win.blit(oran_trans, (x * ctver_vel_x + scr_offset_x, y * ctver_vel_y + scr_offset_y))
            if ctverce[vybrany[1] - 1][vybrany[0] - 2 + div][2] == 0:
                x = ctverce[vybrany[1] - 1][vybrany[0] - 2 + div][0]
                y = ctverce[vybrany[1] - 1][vybrany[0] - 2 + div][1]
                win.blit(oran_trans, (x * ctver_vel_x + scr_offset_x, y * ctver_vel_y + scr_offset_y))
        div = (vybrany[1] + 1) % 2
        if vybrany[0] < radky - 1 and vybrany[1] < sloupce:

            if ctverce[vybrany[1] - 1][vybrany[0] + 2 - div][2] == 0:
                x = ctverce[vybrany[1] - 1][vybrany[0] + 2 - div][0]
                y = ctverce[vybrany[1] - 1][vybrany[0] + 2 - div][1]
                win.blit(oran_trans, (x * ctver_vel_x + scr_offset_x, y * ctver_vel_y + scr_offset_y))
            if ctverce[vybrany[1] + 1][vybrany[0] + 2 - div][2] == 0:
                x = ctverce[vybrany[1] + 1][vybrany[0] + 2 - div][0]
                y = ctverce[vybrany[1] + 1][vybrany[0] + 2 - div][1]
                win.blit(oran_trans, (x * ctver_vel_x + scr_offset_x, y * ctver_vel_y + scr_offset_y))
            if ctverce[vybrany[1]][vybrany[0] + 2][2] == 0:
                x = ctverce[vybrany[1]][vybrany[0] + 2][0]
                y = ctverce[vybrany[1]][vybrany[0] + 2][1]

                win.blit(oran_trans, (x * ctver_vel_x + scr_offset_x, y * ctver_vel_y + scr_offset_y))
    print(vybrany)
    div = 0

    rub_pocet = font_cisel.render(str(pocet_jedn[1]), False, (255, 0, 0))
    perl_pocet = font_cisel.render(str(pocet_jedn[2]), False, (0, 0, 255))

    celk_pocet_poli = pocet_jedn[1] + pocet_jedn[2]
    try:
        pocet_rub_pruh.w = int(pocet_jedn[1] / celk_pocet_poli * screen_x)

        pocet_perl_pruh.x = int(screen_x - ((pocet_jedn[2] / celk_pocet_poli) * screen_x))
        pocet_perl_pruh.w = int(((pocet_jedn[2] / celk_pocet_poli) * screen_x))
        pygame.draw.rect(win, (120, 0, 0), pocet_rub_pruh)
        pygame.draw.rect(win, (0, 0, 120), pocet_perl_pruh)
    except:
        print("nedokazal vypocitat pruhy")
    win.blit(rub_pocet, (0, 0))
    win.blit(perl_pocet, (screen_x - 50, 0))
    if edit:
        run = True
    elif pocet_jedn[1] > pocet_jedn[2] and (pocet_jedn[0] == 0 or konec) or not pocet_jedn[2]:
        win.blit(red_win, (half_x - 300, half_y - 300))
        pygame.mixer.stop()
        kon_sound.play()
    elif pocet_jedn[2] > pocet_jedn[1] and (pocet_jedn[0] == 0 or konec) or not pocet_jedn[2]:
        win.blit(blue_win, (half_x - 300, half_y - 300))
        pygame.mixer.stop()
        kon_sound.play()
    elif pocet_jedn[2] == pocet_jedn[1] and (pocet_jedn[0] == 0 or konec):
        win.blit(remiza, (half_x - 300, half_y - 300))
        pygame.mixer.stop()
        kon_sound.play()


# konec zobraz()
# noinspection PyBroadException
def vytv_kruhy():
    global vsechny_kruhy, radky, sloupce, x, y
    for y in range(radky):
        for x in range(sloupce):
            try:
                vsechny_kruhy.append(
                    [pygame.draw.circle(win, BLUE, (ctverce[x][y][0] * ctver_vel_x + int(ctver_vel_x / 1.5) +
                                                    scr_offset_x, ctverce[x][y][1] * ctver_vel_y +
                                                    int(ctver_vel_y / 2) + scr_offset_y),
                                        int(ctver_vel_x / 2.2)), x, y])
            except:
                print("nemuzu vytvorit kruhy")


# kone

# priprava
vytv_map(True)  # vytvoreni hraci plochy
vytv_kruhy()
kolik_poli()
zobraz()

while v_menu:
    menu()
zobraz()
pygame.display.flip()
while True:

    prem = [[0], [0, 0]]
    konec = False
    klikl = False
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()

        if event.type == pygame.MOUSEBUTTONDOWN:
            coor.clear()
            prem = [[0], [0, 0]]
            for x in range(radky):
                for y in range(sloupce):
                    if ctverce[y][x][2] == hraje:

                        div = (y + 1) % 2
                        try:

                            if ctverce[y][x + 1][2] == 0 or ctverce[y][x - 1][2] == 0:  # vodorovne
                                prem = [[1], [y, x]]
                                print("vodorovne")
                        except IndexError:
                            continue
                        try:
                            if ctverce[y + 1][x - div][2] == 0 or ctverce[y + 1][x + 1 - div][2] == 0:  # sikmo
                                prem = [[1], [y, x]]
                                print("sikmo")
                        except IndexError:
                            continue
                        try:
                            if ctverce[y - 1][x - div][2] == 0 or ctverce[y - 1][x + 1 - div][2] == 0:  # sikmo
                                prem = [[1], [y, x]]
                                print("sikmo dolu")
                        except IndexError:
                            continue
                        try:
                            if ctverce[y + 2][x][2] == 0 or ctverce[y + 2 - div][x + 1][2] == 0 \
                                    or ctverce[y + 2 - div][x - 1][2] == 0:  # o dve nahore
                                prem = [[1], [y, x]]
                                print("o dve nahore")
                        except IndexError:
                            continue
                        try:
                            if ctverce[y][x + 2][2] == 0 or ctverce[y + 1][x + 2 - div][2] == 0 or \
                                    ctverce[y - 1][x + 2 - div][2] == 0:
                                # o dva doleva
                                prem = [[1], [y, x]]
                                print("o dve doleva")
                        except IndexError:
                            continue
                        div = y % 2
                        try:
                            if ctverce[y - 2][x][2] == 0 and y >= 2 or ctverce[y - 2 + div][x + 1][2] == 0 and (
                                    y - 2 + div >= 0 and x < sloupce) or \
                                    ctverce[y - 2 + div][x - 1][
                                        2] == 0 and (y - 2 + div >= 0 and x > 0):  # o dve dolu
                                prem = [[1], [y, x]]
                                print("o dve dolu")
                        except IndexError:
                            continue
                        try:
                            if ctverce[y][x - 2][2] == 0 and x >= 2 or ctverce[y + 1][x - 1 - div][2] == 0 and (
                                    x - 1 - div >= 0 and y < radky) or \
                                    ctverce[y - 1][x - 1 - div][2] == 0 and (x - 1 - div >= 0 and y > 0):
                                # o dve doprava
                                prem = [[1], [y, x]]
                                print("o dve doprava")

                        except IndexError:
                            continue
            if not prem[0][0]:
                konec = True
                pocet_jedn[0] = 0
            prem = [[0], [0, 0]]
            for kruh in vsechny_kruhy:
                if kruh[0].collidepoint(event.pos):
                    coor = [kruh[2], kruh[1]]
            if event.button == 1:
                pos = pygame.mouse.get_pos()
                for b in hra_tl:
                    if b.rect.collidepoint(pos):
                        b.call_back()

            klikl = True
            try:
                if not coor != []:
                    klikl = False
                elif not coor == []:
                    y = coor[0]
                    x = coor[1]
                    div = int((x + 1) % 2)
            except IndexError:
                continue
            if klikl and edit:
                ctverce[x][y][2] = hraje
            if klikl and ctverce[x][y][2] and hraje == 1:
                vybrany = []

                if ctverce[x][y][2] == 1:  # hraje cerveny a klikl na cerveneho
                    vybrany += coor

                klikl = False

            if vybrany != [] and klikl and not ctverce[x][y][2] and hraje == 1:

                if [y + 1, x] == vybrany or [y - 1, x] == vybrany:  # svisle
                    ctverce[x][y][2] = hraje
                    prem = [[1], [0, 0]]
                div = int((x + 1) % 2)
                if [y - div, x - 1] == vybrany or [y + 1 - div, x - 1] == vybrany:  # doprava
                    ctverce[x][y][2] = hraje
                    prem = [[1], [0, 0]]
                if [y - div, x + 1] == vybrany or [y + 1 - div, x + 1] == vybrany:  # doprava
                    ctverce[x][y][2] = hraje
                    prem = [[1], [0, 0]]
                print(div)
                div = int((x + 1) % 2)
                if [y + 2, x] == vybrany or [y + 2 - div, x + 1] == vybrany or [y + 2 - div,
                                                                                x - 1] == vybrany:  # o dve dolu
                    ctverce[x][y][2] = hraje
                    ctverce[vybrany[1]][vybrany[0]][2] = 0
                    prem = [[1], [0, 0]]
                div = int(x % 2)
                if [y - 2, x] == vybrany or [y - 2 + div, x + 1] == vybrany or [y - 2 + div,
                                                                                x - 1] == vybrany:  # o dve dolu
                    ctverce[x][y][2] = hraje
                    ctverce[vybrany[1]][vybrany[0]][2] = 0
                    prem = [[1], [0, 0]]
                # do stran
                div = int((x + 1) % 2)
                if [y, x + 2] == vybrany or [y + 1, x + 2] == vybrany or [y - 1, x + 2] == vybrany:  # o dve doleva
                    ctverce[x][y][2] = hraje
                    ctverce[vybrany[1]][vybrany[0]][2] = 0
                    prem = [[1], [0, 0]]
                if [y, x - 2] == vybrany or [y + 1, x - 2] == vybrany or [y - 1, x - 2] == vybrany:  # o dve doprava
                    ctverce[x][y][2] = hraje
                    ctverce[vybrany[1]][vybrany[0]][2] = 0
                    prem = [[1], [0, 0]]

                div = int((x + 1) % 2)
                if ctverce[x][y + 1][2] == 2 and prem[0][0]:  # nahoru
                    ctverce[x][y + 1][2] = hraje
                if ctverce[x][y - 1][2] == 2 and prem[0][0]:  # dolu
                    ctverce[x][y - 1][2] = hraje

                if ctverce[x - 1][y + 1 - div][2] == 2 and prem[0][0]:  # vlevo dolu
                    ctverce[x - 1][y + 1 - div][2] = hraje
                if ctverce[x - 1][y - div][2] == 2 and prem[0][0]:  # vlevo nahoru
                    ctverce[x - 1][y - div][2] = hraje

                if ctverce[x + 1][y + 1 - div][2] == 2 and prem[0][0]:  # doprava dolu
                    ctverce[x + 1][y + 1 - div][2] = hraje
                if ctverce[x + 1][y - div][2] == 2 and prem[0][0]:  # doprava nahoru
                    ctverce[x + 1][y - div][2] = hraje
                if prem[0][0]:
                    pygame.mixer.music.load(rub_sound)
                    pygame.mixer.music.play()

                vybrany = []
                klikl = False
                if prem[0][0]:
                    hraje = 2
                prem = [[0], [0, 0]]
            if klikl and ctverce[x][y][2] and hraje == 2:
                vybrany = []

                if ctverce[x][y][2] == 2 and hraje == 2:  # hraje cerveny a klikl na cerveneho
                    vybrany += coor
                    klikl = False

            if vybrany != [] and klikl and not ctverce[x][y][2] and hraje == 2:
                if [y + 1, x] == vybrany or [y - 1, x] == vybrany:  # svisle
                    ctverce[x][y][2] = hraje
                    prem = [[1], [0, 0]]
                div = int((x + 1) % 2)
                if [y - div, x - 1] == vybrany or [y + 1 - div, x - 1] == vybrany:  # doprava
                    ctverce[x][y][2] = hraje
                    prem = [[1], [0, 0]]
                if [y - div, x + 1] == vybrany or [y + 1 - div, x + 1] == vybrany:  # doprava
                    ctverce[x][y][2] = hraje
                    prem = [[1], [0, 0]]
                print(div)
                div = int((x + 1) % 2)
                if [y + 2, x] == vybrany or [y + 2 - div, x + 1] == vybrany or [y + 2 - div,
                                                                                x - 1] == vybrany:  # o dve dolu
                    ctverce[x][y][2] = hraje
                    ctverce[vybrany[1]][vybrany[0]][2] = 0
                    prem = [[1], [0, 0]]
                div = int(x % 2)
                if [y - 2, x] == vybrany or [y - 2 + div, x + 1] == vybrany or [y - 2 + div,
                                                                                x - 1] == vybrany:  # o dve dolu
                    ctverce[x][y][2] = hraje
                    ctverce[vybrany[1]][vybrany[0]][2] = 0
                    prem = [[1], [0, 0]]
                # do stran
                div = int((x + 1) % 2)
                if [y, x + 2] == vybrany or [y + 1, x + 2] == vybrany or [y - 1, x + 2] == vybrany:  # o dve doleva
                    ctverce[x][y][2] = hraje
                    ctverce[vybrany[1]][vybrany[0]][2] = 0
                    prem = [[1], [0, 0]]
                if [y, x - 2] == vybrany or [y + 1, x - 2] == vybrany or [y - 1, x - 2] == vybrany:  # o dve doprava
                    ctverce[x][y][2] = hraje
                    ctverce[vybrany[1]][vybrany[0]][2] = 0
                    prem = [[1], [0, 0]]

                div = int((x + 1) % 2)
                if ctverce[x][y + 1][2] == 1 and prem[0][0]:  # nahoru
                    ctverce[x][y + 1][2] = hraje
                if ctverce[x][y - 1][2] == 1 and prem[0][0]:  # dolu
                    ctverce[x][y - 1][2] = hraje

                if ctverce[x - 1][y + 1 - div][2] == 1 and prem[0][0]:  # vlevo dolu
                    ctverce[x - 1][y + 1 - div][2] = hraje
                if ctverce[x - 1][y - div][2] == 1 and prem[0][0]:  # vlevo nahoru
                    ctverce[x - 1][y - div][2] = hraje

                if ctverce[x + 1][y + 1 - div][2] == 1 and prem[0][0]:  # doprava dolu
                    ctverce[x + 1][y + 1 - div][2] = hraje
                if ctverce[x + 1][y - div][2] == 1 and prem[0][0]:  # doprava nahoru
                    ctverce[x + 1][y - div][2] = hraje
                if prem[0][0]:
                    pygame.mixer.music.load(per_sound)
                    pygame.mixer.music.play()

                vybrany = []
                klikl = False
                if prem[0][0] and not hraje == 3:
                    hraje = 1
    kolik_poli()
    zobraz()

    ny = myfont.render(str([hraje]), False, (0, 0, 255))
    # win.blit(ny, (0, 0))  # pozice myši
    for b in hra_tl:
        b.draw(win)
    x, y = pygame.mouse.get_pos()
    x -= int(s_cer_tr.get_width() / 2)
    y -= 10
    if hraje == 1:
        win.blit(s_cer_tr, (x, y))
    if hraje == 2:
        win.blit(s_mod_tr, (x, y))
    pygame.display.flip()  # vykresleni na obrazovku
    while v_menu:
        menu()
        # vykresleni na obrazovku
    clock.tick(60)
